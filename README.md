Obfu9
=====

[![pipeline status](https://gitlab.com/eternal-twin/tools/obfu9/badges/master/pipeline.svg)](https://gitlab.com/eternal-twin/tools/obfu9/-/commits/master)

Bleeding edge builds
--------------------
[Linux (64 bits)](https://gitlab.com/eternal-twin/tools/obfu9/-/jobs/artifacts/master/download?job=build-linux-x86_64)

[Linux (32 bits)](https://gitlab.com/eternal-twin/tools/obfu9/-/jobs/artifacts/master/download?job=build-linux-i686)

[Windows (64 bits)](https://gitlab.com/eternal-twin/tools/obfu9/-/jobs/artifacts/master/download?job=build-windows-x86_64)

[Windows (32 bits)](https://gitlab.com/eternal-twin/tools/obfu9/-/jobs/artifacts/master/download?job=build-windows-i686)

Usage
-----
```
$ ./src/deobfu9_hkey -h
Usage:
    ./src/deobfu9_hkey [-log <stage>[:<sub_stage>[:<step>]]=<level>[,...]] [-i <input_file>] [-o <output_file>]

Read either:
 - from stdin
 - from the specified <input_file>

Output the hkey either:
 - to stdout
 - to the specified <output_file>

Log specifier format:
 Default options: *=trace,*:shuffle=info

 <stage>:
  - load
  - extract_1_4
  - extract_above_4
  - bruteforce
  - *

 <sub_stage>:
  - scheduler
  - main
  - shuffle
  - *

 <step>:
  - kn_above_7f
  - offset
  - main
  - count_missing_entries
  - kxk_from_pxk
  - kxk_from_kxk
  - pxk_from_pxk_x_kxk
  - pxp_from_pxk
  - log_completion
  - check_complete_kxk
  - *

 <level>:
  - silent
  - critical
  - error
  - warn
  - info
  - debug
  - trace

Input file example:

# This is a comment.
# This section should be the first, containing identifiers of increasing size from 1 to 4.
# It is treated in a single pass.
[extract_1_4]

a @\x01 # A comment
b r\x01
d t\x01
as A\x0e\x01
id \x1d\x0b\x01
Num \#Fx\x01
Ray \x1b\x0by\x01
Std \\0F\x01
haxe x+[\x1f\x01
head \x0e\x1aE^\x01
init \x1c[Dl\x01
# The shuffle instruction is required to combine the information extracted from several identifiers.
# Exactly one should be placed at the end of this section.
---shuffle---

# This section should contain at least one identifier per size increasing from 5 to 16.
# It is treated in multiple passes, as long as a change occur.
[extract_above_4]

Bytes \x02\by<\x03
CData G}+n
# There should be a shuffle instruction at the end of a group of the same size.
---shuffle---

BASE64 eH@\x20\x02
Custom v\bT8\x03
---shuffle---

Dynamic 5\x03\x1e'\x03
---shuffle---

# [...]

skip_constructor \x1c\x14'\x01
---shuffle---

# The last section does the actual brute force on the previously extracted data to find the key.
# It should contain two or more entries to ensure that the correct key is found.
[bruteforce]

a @\x01
b r\x01
skip_constructor \x1c\x14'\x01
# End of file

More examples can be found either in the sources or in the distributed compiled package.
```
```
$ ./src/obfu9 -h
Usage:
    ./src/obfu9                 [-k <key> | -hk <hashed_key>] [-t] [-m] [-o <output_file>]
    ./src/obfu9 -i <input_file> [-k <key> | -hk <hashed_key>] [-t] [-m] [-o <output_file>]
    ./src/obfu9 -s <value>      [-k <key> | -hk <hashed_key>] [-t] [-m] [-o <output_file>]

Read either:
 - one identifier per line from stdin
 - one identifier per line from the specified <input_file>
 - specified <value> as a single identifier

Hash either:
 - using default empty key ""
 - using specified <key>
 - using specified MD5 <hashed_key>

Output for each input identifier either:
 - <hashed identifier><EOL> (default)
 - <hashed identifier><SEP><identifier><EOL> (with -m)

Output format:
 - binary with <SEP>=<EOL>='\0' (default)
 - plain text with <SEP>=' ' and <EOL>='\n' (with -t)

Output to either:
 - stdout
 - the specified <output_file>
```
```
$ ./src/obfu9_bf -h
Usage:
    ./src/obfu9_bf -k|-hk <key|hashed_key> -s <hashed_value>  [-charset <charset>]
                                          [-min <min_length>] [-max <max_lenth>]

Defaults:
    <charset>: A-Za-z0-9_
    <min_length>: 1
    <max_length>: 32
```
