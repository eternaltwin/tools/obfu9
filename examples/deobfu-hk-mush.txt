[extract_1_4]

b N
f 3
t X
x \x16
as \x0e\x07\x01
gx =J\x01
id \x02G\x01
io *)\x01
# io *>\x01 # WRONG
me \x02C\x01
mt \x19\x19\x01
nz _k\x01
tf j;\x01
# tf j.\x01 # WRONG
# tf j:\x01 # WRONG
ui '\x0b\x01
# ui "\x0b\x01 # WRONG
Lib i&o\x01
Log ?\x11\x04\x02
Std JVN\x01
add f\x15d\x01
att \x03'\x18\x01
# att \x03"\x18\x01 # WRONG
buf O]j\x01
# cur '\x0bY\x01 # WRONG
cur "\x0bY\x01
get \x20!\x07\x01
has \x19|(\x01
# has \x19|<\x01 # WRONG
pos \n22\x01
run LqT\x01
set 4+l\x01
sub G5J\x01
tag `Os\x01
url ]\x05\x19\x01
xml E\x1af\x01
# Boot qiB; # WRONG
Boot qiB.
# Boot qiB: # WRONG
Data `&`\x01
EReg '\x1cY\x10
# EReg "\x1cY\x10 # WRONG
Fast \x0fFs\x11
Grid ^=Y\x1b
Http \n(='
# Http \n<=' # WRONG
# Http \n(=" # WRONG
# Http \n<=" # WRONG
List P\x17v\x0f
Main 2I,?
# Ship \x14+;K # WRONG
# Ship \x14+.K # WRONG
Ship \x14+:K
# Type ;1\x20\x16 # WRONG
Type .1\x20\x16
# Type :1\x20\x16 # WRONG
blit 9bzG
eval \x17eD\x1d
haxe Uk\x07F
init \x16#w\x17
keys vI*\x03
name HHu\x17
next \x17\x16e\x03\x01
# node ;\x12\x18\x03\x01 # WRONG
node .\x12\x18\x03\x01
# node :\x12\x18\x03\x01 # WRONG
stop '\x11K@
# stop "\x11K@ # WRONG
# wrap y\ty; # WRONG
wrap y\ty.
# wrap y\ty: # WRONG
---shuffle---

[extract_above_4]

Bytes \x17T5\f\x01
CData Km\x03\r
# CODES *[e;\x02 # WRONG
CODES *[e.\x02
# CODES *[e:\x02 # WRONG
Debug aj\x19-\x02
Error \x17L#5\x03
# Timer ;\x0bh\x20\x01 # WRONG
Timer .\x0bh\x20\x01
# Timer :\x0bh\x20\x01 # WRONG
# alloc `),N\x03 # WRONG
alloc `>,N\x03
cache ,Qp\x02\x03
clear \x07$m\x06
field {kk\x18\x02
flash C&\x1a\x03\x01
frame )P\t@\x02
# frame >P\t@\x02 # WRONG
index \x07\x1f;M\x02
# index \x07\x1f.M\x02 # WRONG
# index \x07\x1f:M\x02 # WRONG
lines \x05Jjv\x03
names X(?`\x02
# names X<?`\x02 # WRONG
shash ~0h\x19\x02
stamp \x1b\x05\x11\x02\x01
start \x07X\nz\x01
toHex h\x02!L
trace 7~Zp\x01
wraps z,I1\x02
---shuffle---

BASE64 \f\x0b\x1b\b\x03
Custom R4X\x13\x02
PCData n\x04Y\f\x01
# Planet d\f)\x02\x02 # WRONG
Planet d\f>\x02\x02
# attach \r))@ # WRONG
# attach \r>)@ # WRONG
attach \r)>@
# attach \r>>@ # WRONG
getURL [)(q\x01
# getURL [>(q\x01 # WRONG
# getURL [)<q\x01 # WRONG
# getURL [><q\x01 # WRONG
length \x1dYFp\x03
loader M-LB
params \x15\x15\x044\x01
parent fr@r\x03
scache I7OF\x03
# scount KS;\x02\x02 # WRONG
scount KS.\x02\x02
# scount KS:\x02\x02 # WRONG
string 6hb{\x03
typeof 7(VG\x02
---shuffle---

Comment ,ik+
DocType +p\x19
Dynamic \x177_[\x02
Element 3;~Z\x01
# Element 3.~Z\x01 # WRONG
# Element 3:~Z\x01 # WRONG
# Reflect C\x06\x1f\x01\x02 # WRONG
Reflect C\x06\x1f\x00\x02
current \x1e\x162]\x01
hasNext Dep\x14\x02
matched \b\\\x1az\x02
# measure \\;Z\x01\x01 # WRONG
# measure \\.Z\x01\x01 # WRONG
# measure \\:Z\x01\x01 # WRONG
# measure \\;Z\x00\x01 # WRONG
measure \\.Z\x00\x01
# measure \\:Z\x00\x01 # WRONG
---shuffle---

Document \\\fK\x18\x03
LambdaEx !m0g\x01
elements ^\x03#D\x03
fileName ow\x1ae\x01
getTrace s*,d
iterator \x12^YZ\x01
# resolver w\x16;\x0b\x02 # WRONG
# resolver w\x16.\x0b\x02 # WRONG
resolver w\x16:\x0b\x02
setColor 3ru\x1e\x03
useCache k)p#\x03
# useCache k>p#\x03 # WRONG
wrappers 2-;/\x01
# wrappers 2-./\x01 # WRONG
# wrappers 2-:/\x01 # WRONG
---shuffle---

StringBuf N\x03\x20@\x01
USE_CACHE oUaq\x03
className O;O\x05
# className O.O\x05 # WRONG
# className O:O\x05 # WRONG
initCodes \x11j\f&
lastError 1P'\x05\x01
# lastError 1P"\x05\x01 # WRONG
# remPostFx tA()\x03 # WRONG
# remPostFx tA<)\x03 # WRONG
remPostFx tA(>\x03
# remPostFx tA<>\x03 # WRONG
serialize I2p\\
---shuffle---

FlagsArray r7rx\x01
Serializer AYdV\x02
# lineNumber ;h0y\x03 # WRONG
lineNumber .h0y\x03
# lineNumber :h0y\x03 # WRONG
methodName $+s\x03\x02
parseFloat )Yje\x03
# parseFloat >Yje\x03 # WRONG
---shuffle---

# createCData \x01Qa1\x03 # WRONG
createCData \x00Qa1\x03
doInitDelay 5\x05%_\x01
getNodeType \x0bu\x1ed\x01
# resolveEnum g'v\x0f\x02 # WRONG
resolveEnum g"v\x0f\x02
setResolver \t8\x10t\x03
unserialize \x06!3x
---shuffle---

Unserializer q|Ap\x02
# getClassName IK;@\x02 # WRONG
# getClassName IK.@\x02 # WRONG
getClassName IK:@\x02
resolveClass Ox\x03p\x03
# serializeRef ['%C # WRONG
serializeRef ["%C
useEnumIndex \x12;F{\x01
# useEnumIndex \x12.F{\x01 # WRONG
# useEnumIndex \x12:F{\x01 # WRONG
---shuffle---

createComment ]Kt8\x03
createDocType J\x026W
---shuffle---

USE_ENUM_INDEX \x14VtU
enum_to_string 4gjk\x01
---shuffle---

serializeFields d4\x100\x02
serializeString q$Yi
unserializeEnum h\x129a\x02
---shuffle---

DEFAULT_RESOLVER -Bbg\x01
skip_constructor !3N`\x03
---shuffle---

getEnumConstructs 9T49\x03
unserializeObject D\x0ftW
---shuffle---

[bruteforce]

b N
getEnumConstructs 9T49\x03
