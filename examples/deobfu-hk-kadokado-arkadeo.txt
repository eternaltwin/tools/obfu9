[extract_1_4]

b ^
f `
h k
i W
j 0
n L
q >
# q ) # WRONG
t A
y \x01
Fx \x19f\x01
# bl ;L\x01 # WRONG
bl .L\x01
# bl :L\x01 # WRONG
ds \x163\x01
dx K\x16\x01
dy }i\x01
ex Q\x19\x01
ey &m\x01
ff BL\x01
fx *@\x01
gg r&\x01
id {&\x01
ii \x02a\x01
io \x1f\x12\x01
is Kl\x01
ln )N\x01
# ln >N\x01 # WRONG
me Jg\x01
ms ~\x05\x01
mt aj\x01
sx ra\x01
# sy )+\x01 # WRONG
sy >+\x01
to *&\x01
vr U9\x01
vx b\x03\x02
vy U\x1d\x01
Day 5B\f
Eof Xo\x07
# Lib 7\x1f) # WRONG
Lib 7\x1f>
Log S]_
MD5 1?\x05\x01
Md5 -m\x1e
Std \x14\x11\x04\x01 
add kg\t
# bin \x12\x01E # WRONG
bin \x12\x00E
buf %5\x06\x01 
cmn \x18\ne
frx &rb
fry ][$
get l\x18O
has \x0e]\f
hex \x14'`
# hex \x14"` # WRONG
len K&5
map \x16ws
pin b*r
pop \x1e\x1a\x06\x01
pos it&
rfr dF\x1c
rol \x1fDo
run LkW
set jd\x01
sfr L\bb
sin DI\x01
str F}\x1e
sub \x0b?G
# tag (\x1cL # WRONG
tag <\x1cL
Boot '\x15lZ
# Boot "\x15lZ # WRONG
List Bn\x154
trim \x03@xV
haxe XvJ'
# haxe XvJ" # WRONG
eval 7\\_x
list d\x1b\x20G
rand !W@&
cnew t}\x20-
make \x05\x068m
blit ^\x13y\x19
Type \fV\x10$
# copy "9/r # WRONG
copy '9/r
push ($^Y
# push <$^Y # WRONG
seed $\x03Xd
sign S[@)
# sign S[@> # WRONG
days \x1d}FT
TInt wP%u
Stop f\x15Lt
fold 9d+(
# fold 9d+< # WRONG
iter iNq_
stop \x0b$Gw
root f\x19TE
# keys ;G6, # WRONG
# keys .G6, # WRONG
keys :G6,
MLib \x11}Mn
dead HXj\x0f
coef \x1e15W
glow oh\x04^
blue ja\x01c
# blue ja\x00c # WRONG
from \b|OB
zero fpmt
---shuffle---

[extract_above_4]

TBool \x05ZmN\x01
TNull \x06o\x13w\x03
Timer 92I~\x03
alloc 8{l\n\x03
array l\x1dX\x1f\x01
clear nXB-
count pS\x1d9
first R%\x04O\x01
parse }6\x15a\x03
range \x12\x05DI
right H\x14RL\x02
shash \x0f\x17J\x03
stamp eu\x17Q\x03
toHex 9=AE
trace @+\x06\x0b\x01
value \x10j\fi\x01
---shuffle---

enumEq ^e\x0f7\x01
fields j9Qv\x02
filter CQ\x0e\x02
format \x128]$\x02
ofData h@8M\x03
params \x05]#\x02\x03
remove p\x18\x1f7\x02
scount sT%D
---shuffle---

compare AEeH
---shuffle---

iterator 1Y\x10\x14
initSeed ?fE\x1e\x03
isObject \x1d-\x044
setField L=\\L
allEnums {GO/\x01
hstrings \x0b\bf\x1f\x01
Resource rPo#\x01
getBytes [\x13fG\x02
TreeNode S1,~\x03
# getClass n'AR\x02 # WRONG
getClass n"AR\x02
---shuffle---

DateTools \x0bd@m\x02
---shuffle---

createEnum 5!\x178\x01
---shuffle---

resolveEnum Pt+`\x01
---shuffle---

getClassName /J\x18R
---shuffle---

DAYS_OF_MONTH {861\x03
---shuffle---

createInstance 1qY+
---shuffle---

enumConstructor TsX5\x02
---shuffle---

DEFAULT_RESOLVER \x04\bNN\x02
---shuffle---

[bruteforce]

DEFAULT_RESOLVER \x04\bNN\x02
serializeException $iLL\x03
