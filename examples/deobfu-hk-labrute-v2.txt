[extract_1_4]

b \x11\x01
f d\x01
h 1\x01
q {\x01
# as \x04'\x01 # WRONG
as \x04"\x01
# io %;\x01 # WRONG
# io %.\x01 # WRONG
io %:\x01
is vn\x01
tf y\x13\x01
Lib Y\x14\x1b\x01
NaN 1\x112\x01
Std IN\x19\x01
add 9\x01m\x01
# add 9\x00m\x01 # WRONG
buf be!\x01
doc \x0f|\x01
get \x1a\t\x07\x02
# hex ;;\x0b\x01 # WRONG
hex ;.\x0b\x01
# hex ;:\x0b\x01 # WRONG
# hex .;\x0b\x01 # WRONG
# hex ..\x0b\x01 # WRONG
# hex .:\x0b\x01 # WRONG
# hex :;\x0b\x01 # WRONG
# hex :.\x0b\x01 # WRONG
# hex ::\x0b\x01 # WRONG
map kpF\x01
# max ''\x07\x01 # WRONG
# max '"\x07\x01 # WRONG
max "'\x07\x01
# max ""\x07\x01 # WRONG
min hah\x01
now \x0bG~\x01
pop sF!\x01
ref m72\x01
run m6V\x01
set G\x11L\x01
sub B?\x16\x01
tag lxx\x01
Boot J\x0f\x19y\x01
# Hash HCM)\x01 # WRONG
Hash HCM>\x01
List ~\x15`\x06\x02
TInt Eqim\x01
Type WU\x7f\x15\x01
Void \x19l\x18B\x01
# blit s\x010\x0f\x01 # WRONG
blit s\x000\x0f\x01
copy H\x01YD\x01
# copy H\x00YD\x01 # WRONG
eval uk\x1aZ\x01
haxe x=\x1f\x06\x02
join \x05\x7f\x15J\x01
keys \x1eyC\x1f\x01
last )_A3\x01
# last >_A3\x01 # WRONG
lpad 0Q(U\x01
# lpad 0Q<U\x01 # WRONG
next H\x0bGi\x01
# push g\x05s;\x01 # WRONG
# push g\x05s.\x01 # WRONG
push g\x05s:\x01
rpad \x06Om\x04\x02
trim \x07\x05+e\x01
---shuffle---

[extract_above_4]

Bytes IA\x13-\x02
Error \x01\x0e%\x17\x02
# Error \x00\x0e%\x17\x02 # WRONG
# TBool I;#,\x02 # WRONG
TBool I.#,\x02
# TBool I:#,\x02 # WRONG
TEnum h\x19`c\x01
TNull \x20ePd\x03
alloc \x06T4,
# cache e(|x\x03 # WRONG
cache e<|x\x03
clear S\x11\x07\x1b\x03
field {``c\x01
first \x05\x1b\x06[\x01
flash mWD4\x02
index \x03i\x11\x12
lines (=F0\x02
# lines <=F0\x02 # WRONG
ltrim DaqU\x03
rtrim DXBb\x03
shash l;\b\x0e\x03
# shash l.\b\x0e\x03 # WRONG
# shash l:\b\x0e\x03 # WRONG
# trace q2'2\x02 # WRONG
trace q2"2\x02
---shuffle---

BASE64 ]\x12]\x05
Custom \x11j=4\x01
TClass rML\x01\x02
# TClass rML\x00\x02 # WRONG
TFloat Oti3\x01
typeof F\x11\x1a1
---shuffle---

# Blocked `\x13;W\x01 # WRONG
Blocked `\x13.W\x01
# Blocked `\x13:W\x01 # WRONG
---shuffle---

# Overflow ;~(+\x03 # WRONG
Overflow ;~<+\x03
# Overflow .~(+\x03 # WRONG
# Overflow .~<+\x03 # WRONG
# Overflow :~(+\x03 # WRONG
# Overflow :~<+\x03 # WRONG
---shuffle---

StringBuf k\t8%
---shuffle---

Serializer X/\x1ex
---shuffle---

StringTools \x11\x16\n\x1d\x02
deleteField s5u\x13\x03
# getEnumName \x12\x02(,\x02 # WRONG
getEnumName \x12\x02<,\x02
hxSerialize IU\x17\x15\x01
makeVarArgs G\x1fwd\x03
resolveEnum ^Q\x13\x13\x02
---shuffle---

getClassName \x1b|jc\x01
# htmlUnescape NA)s # WRONG
htmlUnescape NA>s
# serializeRef 4\x1b~\x01\x01 # WRONG
serializeRef 4\x1b~\x00\x01
useEnumIndex \x18r'\x05\x02
# useEnumIndex \x18r"\x05\x02 # WRONG
---shuffle---

OutsideBounds 5\x1d=s
---shuffle---

USE_ENUM_INDEX _\x1e\r4
---shuffle---

enumConstructor WE*i\x01
---shuffle---

skip_constructor x~y\x18\x02
---shuffle---

[bruteforce]

is vn\x01
skip_constructor x~y\x18\x02
serializeClassFields \rB\x06D
