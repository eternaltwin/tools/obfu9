SUBDIRS-y := lib
EXTDIRS-y :=

libobfu9 := lib/libobfu9$(STATIC_LIB_EXT)
obfu9 := obfu9$(EXE_EXT)
obfu9_bf := obfu9_bf$(EXE_EXT)
deobfu9_hkey := deobfu9_hkey$(EXE_EXT)

BINS-y := $(obfu9) $(obfu9_bf) $(deobfu9_hkey)

OBJS-$(obfu9)-y := \
	obfu9.c.o
LIBS-$(obfu9)-y := \
	$(libobfu9)

OBJS-$(obfu9_bf)-y := \
	obfu9_bf.c.o
LIBS-$(obfu9_bf)-y := \
	$(libobfu9)

OBJS-$(deobfu9_hkey)-y := \
	deobfu9_hkey.c.o
LIBS-$(deobfu9_hkey)-y := \
	$(libobfu9)

CFLAGS-y  += -I$(MKS_PROJDIR)/include -I$(MKS_PROJDIR) -I$(MKS_PROJDIR)/libs/$(OS)/include/any
LDFLAGS-y += -L$(MKS_PROJDIR)/libs/$(OS)/x86 -L$(MKS_PROJDIR)/libs/$(OS)/x86_64
LDFLAGS-y += -lcrypto

CFLAGS-$(CONFIG_OS_WINDOWS) += -D__USE_MINGW_ANSI_STDIO=1
LDFLAGS-$(CONFIG_OS_WINDOWS) += -lws2_32
