/*
 * Obfu9
 * Copyright (C) 2020-2022 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#ifndef OBFU9_MACRO_H
#define OBFU9_MACRO_H 1

#define OBFU9_MARK_USED(var) ((void)(var))

#define OBFU9_STR(x) #x
#define OBFU9_STRLEN(x) (sizeof(x) - 1)
#define OBFU9_ARRAY_SIZE(arr) (sizeof(arr) / sizeof((arr)[0]))

#endif /* OBFU9_MACRO_H */
