/*
 * Obfu9
 * Copyright (C) 2020-2022 Guillaume Charifi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OBFU9_HASH_H
#define OBFU9_HASH_H 1

#include <limits.h>
#include <stddef.h>
#include <stdint.h>

#include "hkey.h"

#define OBFU9_HASH_UINT_DIV_CEIL(a, b) (((a) - 1) / (b) + 1)

#define OBFU9_HASH_PACKED_CHAR_BIT 7
#define OBFU9_HASH_PACKED_CHAR_MASK 0x7F
#define OBFU9_HASH_SIZE (OBFU9_HASH_UINT_DIV_CEIL(sizeof(obfu9_hash_packed_t) * CHAR_BIT, OBFU9_HASH_PACKED_CHAR_BIT) + 1 /* '\0' */)

typedef uint32_t obfu9_hash_packed_t;

struct obfu9_hash_context
{
	struct obfu9_hkey_context hkey;
};

#ifdef __cplusplus
extern "C" {
#endif

extern int obfu9_hash_context_init_raw(struct obfu9_hash_context *ctx, uint8_t hkey[OBFU9_HKEY_LEN]);
extern int obfu9_hash_context_init(struct obfu9_hash_context *ctx, const char *key, size_t key_len);

extern obfu9_hash_packed_t obfu9_hash_obfuscate(struct obfu9_hash_context *ctx, const char *identifier, size_t len);
extern obfu9_hash_packed_t obfu9_hash_obfuscate_unfixed(struct obfu9_hash_context *ctx, const char *identifier, size_t len);
extern void obfu9_hash_obfuscate_unpacked(struct obfu9_hash_context *ctx, char buf[OBFU9_HASH_SIZE], const char *identifier, size_t len);
extern obfu9_hash_packed_t obfu9_hash_pack(const char buf[OBFU9_HASH_SIZE]);
extern obfu9_hash_packed_t obfu9_hash_pack_len(const char buf[OBFU9_HASH_SIZE], size_t len);
extern obfu9_hash_packed_t obfu9_hash_packed_fixup(obfu9_hash_packed_t hash);

extern void obfu9_hash_unpack(char buf[OBFU9_HASH_SIZE], obfu9_hash_packed_t src_hash);
extern size_t obfu9_hash_enumerate_unfixed_count(obfu9_hash_packed_t hash);
extern obfu9_hash_packed_t* obfu9_hash_enumerate_unfixed(obfu9_hash_packed_t *buf, obfu9_hash_packed_t hash);

#ifdef __cplusplus
}
#endif

#endif /* OBFU9_HASH_H */
