/*
 * Obfu9
 * Copyright (C) 2020-2022 Guillaume Charifi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <limits.h>
#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "hash.h"
#include "hkey.h"
#include "macro.h"
#include "misc.h"

#include "recover_hkey.h"

#define LOG0(ctx, lvl, fmt) do { \
		if ((lvl) <= (ctx)->log_lvls[(ctx)->stage][(ctx)->sub_stage][(ctx)->step] && (ctx)->log_cb) { \
			(ctx)->log_cb((ctx)->log_cb_priv, (ctx), (lvl), (fmt)); \
		} \
	} while (0)
#define LOG(ctx, lvl, fmt, ...) do { \
		if ((lvl) <= (ctx)->log_lvls[(ctx)->stage][(ctx)->sub_stage][(ctx)->step] && (ctx)->log_cb) { \
			(ctx)->log_cb((ctx)->log_cb_priv, (ctx), (lvl), (fmt), __VA_ARGS__); \
		} \
	} while (0)
#define CRITICAL0(ctx, fmt) LOG0((ctx), OBFU9_RECOVER_HKEY_LOG_LEVEL_CRITICAL, (fmt))
#define CRITICAL(ctx, fmt, ...) LOG((ctx), OBFU9_RECOVER_HKEY_LOG_LEVEL_CRITICAL, (fmt), __VA_ARGS__)
#define ERROR0(ctx, fmt) LOG0((ctx), OBFU9_RECOVER_HKEY_LOG_LEVEL_ERROR, (fmt))
#define ERROR(ctx, fmt, ...) LOG((ctx), OBFU9_RECOVER_HKEY_LOG_LEVEL_ERROR, (fmt), __VA_ARGS__)
#define WARN0(ctx, fmt) LOG0((ctx), OBFU9_RECOVER_HKEY_LOG_LEVEL_WARN, (fmt))
#define WARN(ctx, fmt, ...) LOG((ctx), OBFU9_RECOVER_HKEY_LOG_LEVEL_WARN, (fmt), __VA_ARGS__)
#define INFO0(ctx, fmt) LOG0((ctx), OBFU9_RECOVER_HKEY_LOG_LEVEL_INFO, (fmt))
#define INFO(ctx, fmt, ...) LOG((ctx), OBFU9_RECOVER_HKEY_LOG_LEVEL_INFO, (fmt), __VA_ARGS__)
#define TRACE0(ctx, fmt) LOG0((ctx), OBFU9_RECOVER_HKEY_LOG_LEVEL_TRACE, (fmt))
#define TRACE(ctx, fmt, ...) LOG((ctx), OBFU9_RECOVER_HKEY_LOG_LEVEL_TRACE, (fmt), __VA_ARGS__)

#define ENTER_STAGE(ctx, stg, bak) do { enum obfu9_recover_hkey_stage bak = (ctx)->stage; (ctx)->stage = (stg)
#define SWITCH_STAGE(ctx, stg) do { (ctx)->stage = (stg); } while (0)
#define LEAVE_STAGE(ctx, bak) (ctx)->stage = (bak); } while (0)

#define ENTER_SUB_STAGE(ctx, sstg, bak) do { enum obfu9_recover_hkey_sub_stage bak = (ctx)->sub_stage; (ctx)->sub_stage = (sstg)
#define SWITCH_SUB_STAGE(ctx, sstg) do { (ctx)->sub_stage = (sstg); } while (0)
#define LEAVE_SUB_STAGE(ctx, bak) (ctx)->sub_stage = (bak); } while (0)

#define ENTER_STEP(ctx, stp, bak) do { enum obfu9_recover_hkey_step bak = (ctx)->step; (ctx)->step = (stp)
#define SWITCH_STEP(ctx, stp) do { (ctx)->step = (stp); } while (0)
#define LEAVE_STEP(ctx, bak) (ctx)->step = (bak); } while (0)

/* Maximum number of missing entries for stage_extract_pxk_from_several_missing(). */
/* It should not be increased, because if > 3 almost only duplicates are encountered. */
#define MISSING_COUNT_MAX 3

/* Should be used if there is no overflow (len == 4 without overflow or len <= 3) */
static enum obfu9_recover_hkey_errno stage_extract_pxk_from_h1_4(struct obfu9_recover_hkey_context *ctx, const char *identifier, obfu9_hash_packed_t hash)
{
	enum obfu9_recover_hkey_errno status = OBFU9_RECOVER_HKEY_ESUCCESS;
	size_t len;
	obfu9_hash_packed_t h;
	obfu9_hash_packed_t offset = 0;

	ENTER_SUB_STAGE(ctx, OBFU9_RECOVER_HKEY_SUB_STAGE_MAIN, sstage_bak);
	ENTER_STEP(ctx, OBFU9_RECOVER_HKEY_STEP_MAIN, step_bak);

	INFO(ctx, ">>> %s <<<\n",  __func__);
	INFO(ctx, "identifier = \"%s\"\n", identifier);
	INFO(ctx, "hash = %#x\n", hash);

	len = strlen(identifier);
	if (len < 1 || len > 4)
	{
		ERROR(ctx, "Identifier length == %zu while expected 1 <= length <= 4\n", len);
		status = OBFU9_RECOVER_HKEY_EINVAL;
		goto fail;
	}

	SWITCH_STEP(ctx, OBFU9_RECOVER_HKEY_STEP_KN_ABOVE_7F);

	h = hash;

	for (size_t i = 0; h; i++)
	{
		uint8_t hn = h % 0x83;

		if (i >= len)
		{
			size_t last_idx = len - 1;

			if (!ctx->kn_above_7F[last_idx])
			{
				ctx->kn_above_7F[last_idx] = 1;
				TRACE(ctx, "K[%zu] >= 0x80\n", last_idx);

				ctx->has_data_changed = 1;
			}
			break;
		}

		/*
		 * `hn < 0x80` handles the following case:
		 * If kn >= 0x80, then hn >= 0x80.
		 * But then, hn can be either:
		 *   - hn <  0x83  ->  0x80 <= (hn % 0x83) < 0x83 -> 0x83 must not be substracted from h
		 *   - hn >= 0x83  ->  0x00 <= (hn % 0x83) < 0x80 -> 0x83 must be substracted from h
		 */
		if (ctx->kn_above_7F[i] && hn < 0x80)
			h -= 0x83;
		h /= 0x83;
	}

	SWITCH_STEP(ctx, OBFU9_RECOVER_HKEY_STEP_OFFSET);

	h = hash;

	for (size_t i = 0; i < len; i++)
		offset = offset * 0x83 + identifier[i];

	offset &= UINT32_C(0x7FFFFFFF);

	TRACE(ctx, "offset = %u\n", offset);

	SWITCH_STEP(ctx, OBFU9_RECOVER_HKEY_STEP_MAIN);

	for (size_t i = 0; h; i++)
	{
		uint8_t hn = h % 0x83;
		size_t n_p;
		size_t n_k;

		if (i >= len)
		{
			CRITICAL0(ctx, "Failed to extract the entry (probably overflow or corrupted)\n");
			status = OBFU9_RECOVER_HKEY_EOVERFLOW;
			goto fail;
		}

		/* See above for `hn < 0x80` */
		if (ctx->kn_above_7F[i] && hn < 0x80)
		{
			hn += 0x83;
			h -= 0x83;
		}

		h /= 0x83;

		n_p = (identifier[i] + offset) & 0x7F;
		n_k = i & 0xF;

		TRACE(ctx, "P[(identifier[%zu](%u) + %u) & 0x7F = %zu] ^ K[%zu] = %u\n", i, identifier[i], offset, n_p, n_k, hn);

		if (ctx->pxk[n_p][n_k] >= 0 && ctx->pxk[n_p][n_k] != hn)
		{
			CRITICAL0(ctx, "Conflicting values in pxk\n");
			status = OBFU9_RECOVER_HKEY_ECONFLICT;
			goto fail;
		}

		if (ctx->pxk[n_p][n_k] < 0)
			ctx->has_data_changed = 1;

		ctx->pxk[n_p][n_k] = hn;
	}

fail:
	SWITCH_STEP(ctx, OBFU9_RECOVER_HKEY_STEP_MAIN);

	INFO(ctx, "<<< %s >>>\n",  __func__);

	LEAVE_STEP(ctx, step_bak);
	LEAVE_SUB_STAGE(ctx, sstage_bak);
	return status;
}

static inline int increment_int16_t_array(int16_t i_a[], size_t len, int16_t limit)
{
	for (size_t i = 0; i < len; i++)
	{
		if (i_a[i] < limit)
		{
			i_a[i]++;
			return 0;
		}

		i_a[i] = 0;
	}

	return 1;
}

/* Should be used if there is an overflow (len == 4 with overflow or len >= 5) */
static enum obfu9_recover_hkey_errno stage_extract_pxk_from_several_missing(struct obfu9_recover_hkey_context *ctx, const char *identifier, obfu9_hash_packed_t hash)
{
	enum obfu9_recover_hkey_errno status = OBFU9_RECOVER_HKEY_ESUCCESS;
	size_t len;
	obfu9_hash_packed_t offset;
	size_t missing_count;
	size_t idx_missing[MISSING_COUNT_MAX];
	int16_t hn[MISSING_COUNT_MAX];
	int16_t i_a[MISSING_COUNT_MAX];

	ENTER_SUB_STAGE(ctx, OBFU9_RECOVER_HKEY_SUB_STAGE_MAIN, sstage_bak);
	ENTER_STEP(ctx, OBFU9_RECOVER_HKEY_STEP_MAIN, step_bak);

	INFO(ctx, ">>> %s <<<\n",  __func__);
	INFO(ctx, "identifier = \"%s\"\n", identifier);
	INFO(ctx, "hash = %#x\n", hash);

	len = strlen(identifier);
	if (len < 1)
	{
		ERROR(ctx, "Identifier length == %zu while expected 1 <= length\n", len);
		status = OBFU9_RECOVER_HKEY_EINVAL;
		goto fail;
	}

	SWITCH_STEP(ctx, OBFU9_RECOVER_HKEY_STEP_OFFSET);

	offset = 0;
	for (size_t i = 0; i < len; i++)
		offset = offset * 0x83 + identifier[i];

	offset &= UINT32_C(0x7FFFFFFF);

	SWITCH_STEP(ctx, OBFU9_RECOVER_HKEY_STEP_COUNT_MISSING_ENTRIES);

	missing_count = 0;

	for (size_t i = 0; i < len; i++)
	{
		size_t n_p = (identifier[i] + offset) & 0x7F;
		size_t n_k = i & 0xF;
		int16_t hn;

		hn = ctx->pxk[n_p][n_k];
		if (hn >= 0)
		{
			TRACE(ctx, "P[%zu] ^ K[%zu] = %u\n", n_p, n_k, hn);
			continue;
		}

		TRACE(ctx, "P[%zu] ^ K[%zu] **not set**\n", n_p, n_k);

		if (missing_count < MISSING_COUNT_MAX)
			idx_missing[missing_count] = i;
		missing_count++;
	}

	if (missing_count > MISSING_COUNT_MAX)
	{
		ERROR(ctx, "Missing more than %u entry\n", MISSING_COUNT_MAX);
		status = OBFU9_RECOVER_HKEY_ENOENOUGHENT;
		goto fail;
	}

	if (missing_count == 0)
	{
		INFO0(ctx, "No missing entry, validating by simulating missing entry [0]\n");
		missing_count = 1;
		idx_missing[0] = 0;
	}

	SWITCH_STEP(ctx, OBFU9_RECOVER_HKEY_STEP_MAIN);

	for (size_t i = 0; i < missing_count; i++)
	{
		hn[i] = -1;
		i_a[i] = 0;
	}

	do
	{
		obfu9_hash_packed_t tsth = 0;

		for (size_t ii = 0; ii < len; ii++)
		{
			size_t i = len - 1 - ii;
			size_t n_p = (identifier[i] + offset) & 0x7F;
			size_t n_k = i & 0xF;
			int is_missing = 0;

			tsth *= 0x83;

			for (size_t j = 0; j < missing_count; j++)
			{
				if (i == idx_missing[j])
				{
					is_missing = 1;
					tsth += i_a[j];
					break;
				}
			}

			if (!is_missing)
				tsth += ctx->pxk[n_p][n_k];
		}

		/* tsth = abs(tsth) */
		if (tsth & UINT32_C(0x40000000))
			tsth = ~(tsth - 1);
		tsth &= UINT32_C(0x3FFFFFFF);

		if (tsth != hash)
			continue;

		if (hn[0] >= 0)
		{
			ERROR0(ctx, "Duplicate\n");
			status = OBFU9_RECOVER_HKEY_EDUPLICATE;
			goto fail;
		}

		memcpy(hn, i_a, sizeof(hn));
	}
	while (!increment_int16_t_array(i_a, missing_count, 0xFF));

	if (hn[0] < 0)
	{
		CRITICAL0(ctx, "Not found\n");
		status = OBFU9_RECOVER_HKEY_ENOTFOUND;
		goto fail;
	}

	for (size_t i = 0; i < missing_count; i++)
	{
		size_t n_p = (identifier[idx_missing[i]] + offset) & 0x7F;
		size_t n_k = idx_missing[i] & 0xF;

		TRACE(ctx, "P[(identifier[%zu](%u) + %u) & 0x7F = %zu] ^ K[%zu] = %u\n", idx_missing[i], identifier[idx_missing[i]], offset, n_p, n_k, hn[i]);

		if (ctx->pxk[n_p][n_k] >= 0 && ctx->pxk[n_p][n_k] != hn[i])
		{
			CRITICAL0(ctx, "Conflicting values in pxk\n");
			status = OBFU9_RECOVER_HKEY_ECONFLICT;
			goto fail;
		}

		if (ctx->pxk[n_p][n_k] < 0)
			ctx->has_data_changed = 1;

		ctx->pxk[n_p][n_k] = hn[i];
	}

fail:
	SWITCH_STEP(ctx, OBFU9_RECOVER_HKEY_STEP_MAIN);

	INFO(ctx, "<<< %s >>>\n",  __func__);

	LEAVE_STEP(ctx, step_bak);
	LEAVE_SUB_STAGE(ctx, sstage_bak);
	return status;
}

static enum obfu9_recover_hkey_errno shuffle_kxk_from_pxk(struct obfu9_recover_hkey_context *ctx)
{
	enum obfu9_recover_hkey_errno status = OBFU9_RECOVER_HKEY_ESUCCESS;

	ENTER_STEP(ctx, OBFU9_RECOVER_HKEY_STEP_KXK_FROM_PXK, step_bak);

	INFO(ctx, ">>> %s <<<\n",  __func__);

	for (size_t i = 0; i < OBFU9_ARRAY_SIZE(ctx->pxk); i++)
	{
		for (size_t j = 0; j < OBFU9_ARRAY_SIZE(ctx->pxk[0]); j++)
		{
			for (size_t k = 0; k < OBFU9_ARRAY_SIZE(ctx->pxk[0]); k++)
			{
				int16_t val;

				if (ctx->pxk[i][j] < 0 || ctx->pxk[i][k] < 0)
					continue;

				val = ctx->pxk[i][j] ^ ctx->pxk[i][k];

				TRACE(ctx, "K[%zu] ^ K[%zu] = (P[%zu] ^ K[%zu] = %d) ^ (P[%zu] ^ K[%zu] = %d) = %d\n", j, k, i, j, ctx->pxk[i][j], i, k, ctx->pxk[i][k], val);

				if ((ctx->kxk[j][k] >= 0 && ctx->kxk[j][k] != val)
						|| (ctx->kxk[k][j] >= 0 && ctx->kxk[k][j] != val))
				{
					CRITICAL0(ctx, "Conflicting values in kxk\n");
					status = OBFU9_RECOVER_HKEY_ECONFLICT;
					goto fail;
				}

				if (ctx->kxk[j][k] < 0 || ctx->kxk[k][j] < 0)
					ctx->has_data_changed = 1;

				ctx->kxk[j][k] = val;
				ctx->kxk[k][j] = val;
			}
		}
	}

fail:
	INFO(ctx, "<<< %s >>>\n",  __func__);
	LEAVE_STEP(ctx, step_bak);
	return status;
}

static enum obfu9_recover_hkey_errno shuffle_kxk_from_kxk(struct obfu9_recover_hkey_context *ctx)
{
	enum obfu9_recover_hkey_errno status = OBFU9_RECOVER_HKEY_ESUCCESS;

	ENTER_STEP(ctx, OBFU9_RECOVER_HKEY_STEP_KXK_FROM_KXK, step_bak);

	INFO(ctx, ">>> %s <<<\n",  __func__);

	for (size_t i = 0; i < OBFU9_ARRAY_SIZE(ctx->kxk); i++)
	{
		for (size_t j = 0; j < OBFU9_ARRAY_SIZE(ctx->kxk[0]); j++)
		{
			for (size_t k = 0; k < OBFU9_ARRAY_SIZE(ctx->kxk[0]); k++)
			{
				int16_t val;

				if (ctx->kxk[i][j] < 0 || ctx->kxk[i][k] < 0)
					continue;

				val = ctx->kxk[i][j] ^ ctx->kxk[i][k];

				TRACE(ctx, "K[%zu] ^ K[%zu] = (K[%zu] ^ K[%zu] = %d) ^ (K[%zu] ^ K[%zu] = %d) = %d\n", j, k, i, j, ctx->kxk[i][j], i, k, ctx->kxk[i][k], val);

				if ((ctx->kxk[j][k] >= 0 && ctx->kxk[j][k] != val)
						|| (ctx->kxk[k][j] >= 0 && ctx->kxk[k][j] != val))
				{
					CRITICAL0(ctx, "Conflicting values in kxk\n");
					status = OBFU9_RECOVER_HKEY_ECONFLICT;
					goto fail;
				}

				if (ctx->kxk[j][k] < 0 || ctx->kxk[k][j] < 0)
					ctx->has_data_changed = 1;

				ctx->kxk[j][k] = val;
				ctx->kxk[k][j] = val;
			}
		}
	}

fail:
	INFO(ctx, "<<< %s >>>\n",  __func__);
	LEAVE_STEP(ctx, step_bak);
	return status;
}

static enum obfu9_recover_hkey_errno shuffle_pxk_from_pxk_x_kxk(struct obfu9_recover_hkey_context *ctx)
{
	enum obfu9_recover_hkey_errno status = OBFU9_RECOVER_HKEY_ESUCCESS;

	ENTER_STEP(ctx, OBFU9_RECOVER_HKEY_STEP_PXK_FROM_PXK_X_KXK, step_bak);

	INFO(ctx, ">>> %s <<<\n",  __func__);

	for (size_t i = 0; i < OBFU9_ARRAY_SIZE(ctx->pxk); i++)
	{
		for (size_t j = 0; j < OBFU9_ARRAY_SIZE(ctx->pxk[0]); j++)
		{
			for (size_t k = 0; k < OBFU9_ARRAY_SIZE(ctx->kxk[0]); k++)
			{
				int16_t val;

				if (ctx->pxk[i][j] < 0 || ctx->kxk[j][k] < 0)
					continue;

				val = ctx->pxk[i][j] ^ ctx->kxk[j][k];

				TRACE(ctx, "P[%zu] ^ K[%zu] = (P[%zu] ^ K[%zu] = %d) ^ (K[%zu] ^ K[%zu] = %d) = %d\n", i, k, i, j, ctx->pxk[i][j], j, k, ctx->kxk[j][k], val);

				if (ctx->pxk[i][k] >= 0 && ctx->pxk[i][k] != val)
				{
					CRITICAL0(ctx, "Conflicting values in pxk\n");
					status = OBFU9_RECOVER_HKEY_ECONFLICT;
					goto fail;
				}

				if (ctx->pxk[i][k] < 0)
					ctx->has_data_changed = 1;

				ctx->pxk[i][k] = val;
			}
		}
	}

fail:
	INFO(ctx, "<<< %s >>>\n",  __func__);
	LEAVE_STEP(ctx, step_bak);
	return status;
}

static enum obfu9_recover_hkey_errno shuffle_pxp_from_pxk(struct obfu9_recover_hkey_context *ctx)
{
	enum obfu9_recover_hkey_errno status = OBFU9_RECOVER_HKEY_ESUCCESS;

	ENTER_STEP(ctx, OBFU9_RECOVER_HKEY_STEP_PXP_FROM_PXK, step_bak);

	INFO(ctx, ">>> %s <<<\n",  __func__);

	for (size_t i = 0; i < OBFU9_ARRAY_SIZE(ctx->pxk); i++)
	{
		for (size_t j = 0; j < OBFU9_ARRAY_SIZE(ctx->pxk[0]); j++)
		{
			for (size_t k = 0; k < OBFU9_ARRAY_SIZE(ctx->pxk); k++)
			{
				int16_t val;

				if (ctx->pxk[i][j] < 0 || ctx->pxk[k][j] < 0)
					continue;

				val = ctx->pxk[i][j] ^ ctx->pxk[k][j];

				TRACE(ctx, "P[%zu] ^ P[%zu] = (P[%zu] ^ K[%zu] = %d) ^ (P[%zu] ^ K[%zu] = %d) = %d\n", i, k, i, j, ctx->pxk[i][j], k, j, ctx->pxk[k][j], val);

				if ((ctx->pxp[i][k] >= 0 && ctx->pxp[i][k] != val)
						|| (ctx->pxp[k][i] >= 0 && ctx->pxp[k][i] != val))
				{
					CRITICAL0(ctx, "Conflicting values in pxp\n");
					status = OBFU9_RECOVER_HKEY_ECONFLICT;
					goto fail;
				}

				if (ctx->pxp[i][k] < 0 || ctx->pxp[k][i] < 0)
					ctx->has_data_changed = 1;

				ctx->pxp[i][k] = val;
				ctx->pxp[k][i] = val;
			}
		}
	}

fail:
	INFO(ctx, "<<< %s >>>\n",  __func__);
	LEAVE_STEP(ctx, step_bak);
	return status;
}

static void shuffle_log_completion(struct obfu9_recover_hkey_context *ctx)
{
	size_t compl;

	ENTER_STEP(ctx, OBFU9_RECOVER_HKEY_STEP_LOG_COMPLETION, step_bak);

	INFO(ctx, ">>> %s <<<\n",  __func__);

	compl = 0;
	for (size_t i = 0; i < OBFU9_ARRAY_SIZE(ctx->kxk); i++)
		for (size_t j = 0; j < OBFU9_ARRAY_SIZE(ctx->kxk[0]); j++)
			if (ctx->kxk[i][j] >= 0)
				compl++;

	INFO(ctx, "kxk: %zu/%zu\n", compl, OBFU9_ARRAY_SIZE(ctx->kxk) * OBFU9_ARRAY_SIZE(ctx->kxk[0]));

	compl = 0;
	for (size_t i = 0; i < OBFU9_ARRAY_SIZE(ctx->pxk); i++)
		for (size_t j = 0; j < OBFU9_ARRAY_SIZE(ctx->pxk[0]); j++)
			if (ctx->pxk[i][j] >= 0)
				compl++;

	INFO(ctx, "pxk: %zu/%zu\n", compl, OBFU9_ARRAY_SIZE(ctx->pxk) * OBFU9_ARRAY_SIZE(ctx->pxk[0]));

	compl = 0;
	for (size_t i = 0; i < OBFU9_ARRAY_SIZE(ctx->pxp); i++)
		for (size_t j = 0; j < OBFU9_ARRAY_SIZE(ctx->pxp[0]); j++)
			if (ctx->pxp[i][j] >= 0)
				compl++;

	INFO(ctx, "pxp: %zu/%zu\n", compl, OBFU9_ARRAY_SIZE(ctx->pxp) * OBFU9_ARRAY_SIZE(ctx->pxp[0]));

	TRACE0(ctx, "kxk contents:\n");
	for (size_t i = 0; i < OBFU9_ARRAY_SIZE(ctx->kxk); i++)
		for (size_t j = i; j < OBFU9_ARRAY_SIZE(ctx->kxk[0]); j++)
			if (ctx->kxk[i][j] > 0)
				TRACE(ctx, "K[%zu] ^ K[%zu] = %d\n", i, j, ctx->kxk[i][j]);

	TRACE0(ctx, "pxk contents:\n");
	for (size_t i = 0; i < OBFU9_ARRAY_SIZE(ctx->pxk); i++)
		for (size_t j = i; j < OBFU9_ARRAY_SIZE(ctx->pxk[0]); j++)
			if (ctx->pxk[i][j] > 0)
				TRACE(ctx, "P[%zu] ^ K[%zu] = %d\n", i, j, ctx->pxk[i][j]);

	TRACE0(ctx, "pxp contents:\n");
	for (size_t i = 0; i < OBFU9_ARRAY_SIZE(ctx->pxp); i++)
		for (size_t j = i; j < OBFU9_ARRAY_SIZE(ctx->pxp[0]); j++)
			if (ctx->pxp[i][j] > 0)
				TRACE(ctx, "P[%zu] ^ P[%zu] = %d\n", i, j, ctx->pxp[i][j]);

	INFO(ctx, "<<< %s >>>\n",  __func__);
	LEAVE_STEP(ctx, step_bak);
}

static enum obfu9_recover_hkey_errno stage_shuffle(struct obfu9_recover_hkey_context *ctx)
{
	enum obfu9_recover_hkey_errno status;

	ENTER_SUB_STAGE(ctx, OBFU9_RECOVER_HKEY_SUB_STAGE_SHUFFLE, sstage_bak);

	shuffle_log_completion(ctx);

	status = shuffle_kxk_from_pxk(ctx);
	if (status != OBFU9_RECOVER_HKEY_ESUCCESS)
		return status;
	shuffle_log_completion(ctx);

	status = shuffle_kxk_from_kxk(ctx);
	if (status != OBFU9_RECOVER_HKEY_ESUCCESS)
		return status;
	shuffle_log_completion(ctx);

	status = shuffle_pxk_from_pxk_x_kxk(ctx);
	if (status != OBFU9_RECOVER_HKEY_ESUCCESS)
		return status;
	shuffle_log_completion(ctx);

	status = shuffle_pxp_from_pxk(ctx);
	if (status != OBFU9_RECOVER_HKEY_ESUCCESS)
		return status;
	shuffle_log_completion(ctx);

	LEAVE_SUB_STAGE(ctx, sstage_bak);
	return OBFU9_RECOVER_HKEY_ESUCCESS;
}

static enum obfu9_recover_hkey_errno stage_bruteforce(struct obfu9_recover_hkey_context *ctx, const char *identifier, obfu9_hash_packed_t hash)
{
	enum obfu9_recover_hkey_errno status = OBFU9_RECOVER_HKEY_ESUCCESS;
	size_t len;
	int found = 0;
	const char *hkey;

	ENTER_SUB_STAGE(ctx, OBFU9_RECOVER_HKEY_SUB_STAGE_MAIN, sstage_bak);
	ENTER_STEP(ctx, OBFU9_RECOVER_HKEY_STEP_MAIN, step_bak);

	INFO(ctx, ">>> %s <<<\n",  __func__);
	INFO(ctx, "identifier = \"%s\"\n", identifier);
	INFO(ctx, "hash = %#x\n", hash);

	SWITCH_STEP(ctx, OBFU9_RECOVER_HKEY_STEP_CHECK_COMPLETE_KXK);

	for (size_t i = 1; i < OBFU9_ARRAY_SIZE(ctx->kxk[0]); i++)
	{
		if (ctx->kxk[0][i] < 0)
		{
			CRITICAL0(ctx, "Incomplete kxk\n");
			status = OBFU9_RECOVER_HKEY_ENOENOUGHENT;
			goto fail;
		}
	}

	SWITCH_STEP(ctx, OBFU9_RECOVER_HKEY_STEP_MAIN);

	len = strlen(identifier);

	for (size_t i = 0; i <= 0xFF; i++)
	{
		uint8_t hk[OBFU9_HKEY_LEN];
		struct obfu9_hash_context obfu;
		obfu9_hash_packed_t h;

		hk[0] = i;
		for (size_t j = 1; j < OBFU9_ARRAY_SIZE(hk); j++)
			hk[j] = hk[0] ^ ctx->kxk[0][j];

		obfu9_hash_context_init_raw(&obfu, hk);
		h = obfu9_hash_obfuscate(&obfu, identifier, len);

		if (h != hash)
			continue;

		if (found)
		{
			ERROR0(ctx, "Duplicate hkey\n");
			status = OBFU9_RECOVER_HKEY_EDUPLICATE;
			goto fail;
		}

		memcpy(ctx->hkey, hk, sizeof(hk));
		found = 1;
	}

	if (!found)
	{
		CRITICAL0(ctx, "Hkey not found\n");
		status = OBFU9_RECOVER_HKEY_ENOTFOUND;
		goto fail;
	}

	hkey = ctx->hkey;
	INFO(ctx, "Hkey found: %.2hhx%.2hhx%.2hhx%.2hhx%.2hhx%.2hhx%.2hhx%.2hhx"
			"%.2hhx%.2hhx%.2hhx%.2hhx%.2hhx%.2hhx%.2hhx%.2hhx\n",
			hkey[0], hkey[1], hkey[2],  hkey[3],  hkey[4],  hkey[5],  hkey[6],  hkey[7],
			hkey[8], hkey[9], hkey[10], hkey[11], hkey[12], hkey[13], hkey[14], hkey[15]);

	ctx->has_data_changed = 1;

fail:
	SWITCH_STEP(ctx, OBFU9_RECOVER_HKEY_STEP_MAIN);

	INFO(ctx, "<<< %s >>>\n",  __func__);

	LEAVE_STEP(ctx, step_bak);
	LEAVE_SUB_STAGE(ctx, sstage_bak);
	return status;
}

void obfu9_recover_hkey_context_reset(struct obfu9_recover_hkey_context *ctx)
{
	for (size_t i = 0; i < OBFU9_ARRAY_SIZE(ctx->kxk); i++)
		for (size_t j = 0; j < OBFU9_ARRAY_SIZE(ctx->kxk[0]); j++)
			ctx->kxk[i][j] = -1;

	for (size_t i = 0; i < OBFU9_ARRAY_SIZE(ctx->pxk); i++)
		for (size_t j = 0; j < OBFU9_ARRAY_SIZE(ctx->pxk[0]); j++)
			ctx->pxk[i][j] = -1;

	for (size_t i = 0; i < OBFU9_ARRAY_SIZE(ctx->pxp); i++)
		for (size_t j = 0; j < OBFU9_ARRAY_SIZE(ctx->pxp[0]); j++)
			ctx->pxp[i][j] = -1;

	for (size_t i = 0; i < OBFU9_ARRAY_SIZE(ctx->kn_above_7F); i++)
		ctx->kn_above_7F[i] = 0;

	for (size_t i = 0; i < OBFU9_RECOVER_HKEY_STAGE_MAX; i++)
		for (size_t j = 0; j < OBFU9_RECOVER_HKEY_SUB_STAGE_MAX; j++)
			for (size_t k = 0; k < OBFU9_RECOVER_HKEY_STEP_MAX; k++)
				ctx->log_lvls[i][j][k] = OBFU9_RECOVER_HKEY_LOG_LEVEL_INFO;

	ctx->log_cb = (obfu9_recover_hkey_log_cb_t)NULL;
	ctx->log_cb_priv = NULL;
	ctx->stage = OBFU9_RECOVER_HKEY_STAGE_INVAL;
	ctx->sub_stage = OBFU9_RECOVER_HKEY_SUB_STAGE_INVAL;
	ctx->step = OBFU9_RECOVER_HKEY_STEP_INVAL;

	while (ctx->match_count--)
		free(ctx->matches[ctx->match_count].identifier);
	ctx->match_count = 0;

	if (ctx->matches)
		free(ctx->matches);
	ctx->matches = NULL;

	ctx->stage_iteration_index = 0;
	ctx->has_data_changed = 0;
}

void obfu9_recover_hkey_context_init(struct obfu9_recover_hkey_context *ctx)
{
	ctx->match_count = 0;
	ctx->matches = NULL;
	obfu9_recover_hkey_context_reset(ctx);
}

void obfu9_recover_hkey_set_log_callback(struct obfu9_recover_hkey_context *ctx, obfu9_recover_hkey_log_cb_t cb, void *cb_priv)
{
	ctx->log_cb = cb;
	ctx->log_cb_priv = cb_priv;
}

static enum obfu9_recover_hkey_errno obfu9_recover_hkey_append(struct obfu9_recover_hkey_context *ctx, enum obfu9_recover_hkey_match_type type, const char *identifier, obfu9_hash_packed_t hash)
{
	enum obfu9_recover_hkey_errno status = OBFU9_RECOVER_HKEY_ESUCCESS;
	char *id;
	struct obfu9_recover_hkey_match *matches;
	struct obfu9_recover_hkey_match *match;

	ENTER_STAGE(ctx, OBFU9_RECOVER_HKEY_STAGE_LOAD, stage_bak);
	ENTER_SUB_STAGE(ctx, OBFU9_RECOVER_HKEY_SUB_STAGE_MAIN, sstage_bak);
	ENTER_STEP(ctx, OBFU9_RECOVER_HKEY_STEP_MAIN, step_bak);

	INFO(ctx, ">>> %s <<<\n",  __func__);
	INFO(ctx, "type = %d\n", type);
	if (identifier)
	{
		INFO(ctx, "identifier = \"%s\"\n", identifier);
		INFO(ctx, "hash = %#x\n", hash);
	}

	id = NULL;

	if (identifier)
	{
		id = obfu9_strdup(identifier);
		if (!id)
		{
			CRITICAL0(ctx, "Identifier allocation failed\n");
			status = OBFU9_RECOVER_HKEY_ENOMEM;
			goto fail;
		}
	}

	matches = realloc(ctx->matches, (ctx->match_count + 1) * sizeof(ctx->matches[0]));
	if (!matches)
	{
		CRITICAL0(ctx, "Matches array allocation failed\n");
		status = OBFU9_RECOVER_HKEY_ENOMEM;
		goto fail;
	}
	ctx->matches = matches;

	match = &ctx->matches[ctx->match_count];
	match->type = type;
	match->identifier = id;
	match->hash = hash;
	ctx->match_count++;

fail:
	INFO(ctx, "<<< %s >>>\n",  __func__);

	LEAVE_STEP(ctx, step_bak);
	LEAVE_SUB_STAGE(ctx, sstage_bak);
	LEAVE_STAGE(ctx, stage_bak);
	return status;
}

enum obfu9_recover_hkey_errno obfu9_recover_hkey_append_match(struct obfu9_recover_hkey_context *ctx, const char *identifier, obfu9_hash_packed_t hash)
{
	return obfu9_recover_hkey_append(ctx, OBFU9_RECOVER_HKEY_MATCH_TYPE_MATCH, identifier, hash);
}

enum obfu9_recover_hkey_errno obfu9_recover_hkey_append_marker_extract_1_4(struct obfu9_recover_hkey_context *ctx)
{
	return obfu9_recover_hkey_append(ctx, OBFU9_RECOVER_HKEY_MATCH_TYPE_MARKER_EXTRACT_1_4, NULL, 0);
}

enum obfu9_recover_hkey_errno obfu9_recover_hkey_append_marker_extract_above_4(struct obfu9_recover_hkey_context *ctx)
{
	return obfu9_recover_hkey_append(ctx, OBFU9_RECOVER_HKEY_MATCH_TYPE_MARKER_EXTRACT_ABOVE_4, NULL, 0);
}

enum obfu9_recover_hkey_errno obfu9_recover_hkey_append_marker_bruteforce(struct obfu9_recover_hkey_context *ctx)
{
	return obfu9_recover_hkey_append(ctx, OBFU9_RECOVER_HKEY_MATCH_TYPE_MARKER_BRUTEFORCE, NULL, 0);
}

enum obfu9_recover_hkey_errno obfu9_recover_hkey_append_marker_shuffle(struct obfu9_recover_hkey_context *ctx)
{
	return obfu9_recover_hkey_append(ctx, OBFU9_RECOVER_HKEY_MATCH_TYPE_MARKER_SHUFFLE, NULL, 0);
}

enum obfu9_recover_hkey_errno obfu9_recover_hkey_run(struct obfu9_recover_hkey_context *ctx)
{
	enum obfu9_recover_hkey_errno status;
	size_t cur_stage_idx = 0;

	/* Default initial stage is extract_1_4 */
	ENTER_STAGE(ctx, OBFU9_RECOVER_HKEY_STAGE_EXTRACT_1_4, stage_bak);
	ENTER_SUB_STAGE(ctx, OBFU9_RECOVER_HKEY_SUB_STAGE_SCHEDULER, sstage_bak);
	ENTER_STEP(ctx, OBFU9_RECOVER_HKEY_STEP_MAIN, step_bak);

	INFO(ctx, ">>> %s <<<\n",  __func__);

	while (cur_stage_idx < ctx->match_count)
	{
		enum obfu9_recover_hkey_stage next_stage = OBFU9_RECOVER_HKEY_STAGE_INVAL;
		size_t next_stage_idx = cur_stage_idx;

		INFO(ctx, "Stage type %u offset %zu iteration %zu\n", ctx->stage, cur_stage_idx, ctx->stage_iteration_index);

		ctx->has_data_changed = 0;

		for (size_t i = cur_stage_idx; i < ctx->match_count; i++)
		{
			struct obfu9_recover_hkey_match *match = &ctx->matches[i];

			next_stage_idx = i + 1;

			if (match->type == OBFU9_RECOVER_HKEY_MATCH_TYPE_MARKER_EXTRACT_1_4)
			{
				next_stage = OBFU9_RECOVER_HKEY_STAGE_EXTRACT_1_4;
				break;
			}
			else if (match->type == OBFU9_RECOVER_HKEY_MATCH_TYPE_MARKER_EXTRACT_ABOVE_4)
			{
				next_stage = OBFU9_RECOVER_HKEY_STAGE_EXTRACT_ABOVE_4;
				break;
			}
			else if (match->type == OBFU9_RECOVER_HKEY_MATCH_TYPE_MARKER_BRUTEFORCE)
			{
				next_stage = OBFU9_RECOVER_HKEY_STAGE_BRUTEFORCE;
				break;
			}
			else if (match->type == OBFU9_RECOVER_HKEY_MATCH_TYPE_MARKER_SHUFFLE)
			{
				status = stage_shuffle(ctx);
				if (status != OBFU9_RECOVER_HKEY_ESUCCESS)
					goto fail;
				continue;
			}

			if (ctx->stage == OBFU9_RECOVER_HKEY_STAGE_EXTRACT_1_4)
			{
				status = stage_extract_pxk_from_h1_4(ctx, match->identifier, match->hash);
				if (status == OBFU9_RECOVER_HKEY_EINVAL)
					continue;
				if (status != OBFU9_RECOVER_HKEY_ESUCCESS)
					goto fail;
			}
			else if (ctx->stage == OBFU9_RECOVER_HKEY_STAGE_EXTRACT_ABOVE_4)
			{
				status = stage_extract_pxk_from_several_missing(ctx, match->identifier, match->hash);
				if (status == OBFU9_RECOVER_HKEY_EDUPLICATE
						|| status == OBFU9_RECOVER_HKEY_ENOENOUGHENT)
					continue;
				if (status != OBFU9_RECOVER_HKEY_ESUCCESS)
					goto fail;
			}
			else if (ctx->stage == OBFU9_RECOVER_HKEY_STAGE_BRUTEFORCE)
			{
				status = stage_bruteforce(ctx, match->identifier, match->hash);
				if (status == OBFU9_RECOVER_HKEY_EDUPLICATE)
					continue;
				if (status != OBFU9_RECOVER_HKEY_ESUCCESS)
					goto fail;
			}
		}

		if (ctx->stage == OBFU9_RECOVER_HKEY_STAGE_EXTRACT_ABOVE_4
				&& ctx->has_data_changed)
		{
			ctx->stage_iteration_index++;
			continue;
		}

		if (next_stage != OBFU9_RECOVER_HKEY_STAGE_INVAL)
			SWITCH_STAGE(ctx, next_stage);
		cur_stage_idx = next_stage_idx;
		ctx->stage_iteration_index = 0;
	}

	status = OBFU9_RECOVER_HKEY_ESUCCESS;

fail:
	INFO(ctx, "<<< %s >>>\n",  __func__);

	LEAVE_STEP(ctx, step_bak);
	LEAVE_SUB_STAGE(ctx, sstage_bak);
	LEAVE_STAGE(ctx, stage_bak);
	return status;
}
