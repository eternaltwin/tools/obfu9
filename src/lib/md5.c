/*
 * Obfu9
 * Copyright (C) 2020-2022 Guillaume Charifi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdarg.h>
#include <stddef.h>
#include <stdio.h>

#include <openssl/evp.h>

#include "md5.h"

#define MD5_HASH_SIZE 16

int obfu9_md5(void *hash, const void *data, size_t size)
{
	EVP_Digest(data, size, (unsigned char *)hash, NULL, EVP_md5(), NULL);

#if 0
	for (size_t i = 0; i < MD5_HASH_SIZE; i++)
		printf("%2.2x", ((unsigned char *)hash)[i]);
	printf("\n");
#endif

	return 0;
}

int obfu9_md5_multipart(void *hash, ...)
{
	int status = -1;
	int result;
	EVP_MD_CTX *evp_ctx;
	va_list ap;

	evp_ctx = EVP_MD_CTX_new();
	if (!evp_ctx)
		goto failed_ctx_new;

	result = EVP_DigestInit(evp_ctx, EVP_md5());
	if (!result)
		goto failed_init;

	va_start(ap, hash);

	result = 1;
	do
	{
		const void *data;
		size_t size;

		data = va_arg(ap, void *);
		if (!data)
			break;

		size = va_arg(ap, size_t);
		if (size == 0)
			continue;

		result = EVP_DigestUpdate(evp_ctx, data, size);
	}
	while (result);

	va_end(ap);

	if (!result)
		goto failed_update;

	result = EVP_DigestFinal(evp_ctx, hash, NULL);
	if (!result)
		goto failed_final;

#if 0
	for (size_t i = 0; i < MD5_HASH_SIZE; i++)
		printf("%2.2x", ((unsigned char *)hash)[i]);
	printf("\n");
#endif

	status = 0;

failed_final:
failed_update:
failed_init:
	EVP_MD_CTX_free(evp_ctx);

failed_ctx_new:
	return status;
}
