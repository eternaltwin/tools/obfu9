/*
 * Obfu9
 * Copyright (C) 2020-2022 Guillaume Charifi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OBFU9_MD5_H
#define OBFU9_MD5_H 1

#include <stddef.h>

#define OBFU9_MD5_HASH_SIZE 16

#ifdef __cplusplus
extern "C" {
#endif

extern int obfu9_md5(void *hash, const void *data, size_t size);
extern int obfu9_md5_multipart(void *hash, ...);

#ifdef __cplusplus
}
#endif

#endif /* OBFU9_MD5_H */
