/*
 * Obfu9
 * Copyright (C) 2020-2022 Guillaume Charifi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <errno.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "lib/hash.h"
#include "lib/opt.h"


static int print_usage(int argc, char **argv)
{
	const char *argv0 = argc >= 1 ? argv[0] : "obfu9_bf";
	size_t argv0_len = strlen(argv0);

	fprintf(stderr, "Usage:\n"
			"    %s -k|-hk <key|hashed_key> -s <hashed_value>  [-charset <charset>]\n"
			"    %*s                        [-min <min_length>] [-max <max_lenth>]\n"
			"\n"
			"Defaults:\n"
			"    <charset>: A-Za-z0-9_\n"
			"    <min_length>: 1\n"
			"    <max_length>: 32\n",
			argv0,
			(int)argv0_len, "");
	return 1;
}

static void bruteforce(struct obfu9_hash_context *ctx, const char *hash, const char *charset, size_t min, size_t max)
{
	int result;
	char hbuf[OBFU9_HASH_SIZE];
	char *buf;
	size_t *bufi;
	size_t charset_len;
	size_t cur = min;

	buf = calloc(max + 1, sizeof(buf[0]));
	bufi = calloc(max, sizeof(bufi[0]));
	charset_len = strlen(charset);

	memset(buf, charset[0], max);

	while (cur <= max)
	{
		int hold = 1;
		const char *p;

		for (size_t ii = 0; ii < cur; ii++)
		{
			size_t i = max - ii - 1;

			if (!hold)
				break;
			hold = 0;

			buf[i] = charset[bufi[i]];

			bufi[i]++;
			if (bufi[i] >= charset_len)
			{
				bufi[i] = 0;
				hold = 1;
			}
		}

		p = &buf[max - cur];

		obfu9_hash_obfuscate_unpacked(ctx, hbuf, p, cur);
		result = strcmp(hbuf, hash);
		if (result == 0)
			printf("Found: \"%s\"\n", p);

		if (hold)
		{
			fprintf(stderr, "Length %zu done.\n", cur);
			cur++;
		}
	}
}

enum opt_id
{
	OPT_CHARSET = 0,
	OPT_HK,
	OPT_K,
	OPT_MAX,
	OPT_MIN,
	OPT_S,
	OPT__END_
};

static inline int ctox(char c) {
	if (c >= '0' && c <= '9')
		return c - '0';
	if (c >= 'A' && c <= 'F')
		return 0xA + c - 'A';
	if (c >= 'a' && c <= 'f')
		return 0xA + c - 'a';
	return INT_MAX;
}

int main(int argc, char **argv)
{
	int status = 1;
	struct obfu9_opt opt_tpl[] = {
		OBFU9_OPT_STR(OPT_CHARSET, "-charset"),
		OBFU9_OPT_STR(OPT_HK,      "-hk"),
		OBFU9_OPT_STR(OPT_K,       "-k"),
		OBFU9_OPT_LONG(OPT_MAX,    "-max"),
		OBFU9_OPT_LONG(OPT_MIN,    "-min"),
		OBFU9_OPT_STR(OPT_S,       "-s"),
		OBFU9_OPT_END(OPT__END_)
	};
	struct obfu9_opt *opts;
	struct obfu9_hash_context ctx;
	const char *charset;
	const char *hk;
	const char *key;
	long max;
	long min;
	const char *hash;

	/*** Handle options ***/

	opts = obfu9_opt_parse(argc, argv, opt_tpl);
	if (!opts)
		return print_usage(argc, argv);

	if (!(!!obfu9_opt_get_defined(opts, OPT_HK) ^ !!obfu9_opt_get_defined(opts, OPT_K)))
	{
		fprintf(stderr, "*ERR*: Exactly one of the options -hk and -k must be given at a time.\n\n");
		return print_usage(argc, argv);
	}

	if (!obfu9_opt_get_defined(opts, OPT_S))
	{
		fprintf(stderr, "*ERR*: Missing required option -s.\n\n");
		return print_usage(argc, argv);
	}

	charset = obfu9_opt_get_str_or(opts, OPT_CHARSET, "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ_abcdefghijklmnopqrstuvwxyz");
	hk      = obfu9_opt_get_str_or(opts, OPT_HK, NULL);
	key     = obfu9_opt_get_str_or(opts, OPT_K, "");
	max     = obfu9_opt_get_long_or(opts, OPT_MAX, 32);
	min     = obfu9_opt_get_long_or(opts, OPT_MIN, 1);
	hash    = obfu9_opt_get_str_or(opts, OPT_S, NULL);

	if (hk)
	{
		size_t hk_len;

		hk_len = strlen(hk);
		if (hk_len != OBFU9_HKEY_LEN * 2)
		{
			fprintf(stderr, "*ERR*: Invalid hkey supplied: length %zu != expected %d.\n\n", hk_len, OBFU9_HKEY_LEN * 2);
			return print_usage(argc, argv);
		}

		for (size_t i = 0; i < hk_len; i++)
		{
			if (ctox(hk[i]) != INT_MAX)
				continue;

			fprintf(stderr, "*ERR*: Invalid hkey supplied: invalid hex char '%c' at index %zu.\n\n", hk[i], i);
			return print_usage(argc, argv);
		}
	}

	if (!charset[0])
	{
		fprintf(stderr, "*ERR*: Empty charset supplied.\n\n");
		return print_usage(argc, argv);
	}

	if (min <= 0)
	{
		fprintf(stderr, "*ERR*: Invalid min length <= 0 supplied.\n\n");
		return print_usage(argc, argv);
	}

	if (max < min)
	{
		fprintf(stderr, "*ERR*: Invalid supplied max length < min length.\n\n");
		return print_usage(argc, argv);
	}

	/*** Compute hashes ***/

	if (hk)
	{
		uint8_t hkey[OBFU9_HKEY_LEN];

		for (size_t i = 0; i < OBFU9_HKEY_LEN; i++)
			hkey[i] = (ctox(hk[i * 2]) << 4) | ctox(hk[i * 2 + 1]);

		obfu9_hash_context_init_raw(&ctx, hkey);
	}
	else
		obfu9_hash_context_init(&ctx, key, strlen(key));

	bruteforce(&ctx, hash, charset, min, max);

	status = 0;

	return status;
}
